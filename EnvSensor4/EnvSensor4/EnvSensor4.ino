#include <PubSubClient.h>
#include "bsec.h"
#include <WiFi.h>
#include <EEPROM.h>
#include <EnvironmentCalculations.h>

// Controls how often the code persists the BSEC Calibration data
#define STATE_SAVE_PERIOD  UINT32_C(60 * 60 * 1000) // every 60 minutes

const char* mqtt_server = "10.1.1.3";
const char *SSID = "Radio-Noise S";
const char *WiFiPassword = "z4!9$*3wb3xxpyv2@gio";
const char *device = "Office";
#define WIFI_TIMEOUT_MS 10000 // 10 second WiFi connection timeout
#define WIFI_RECOVER_TIME_MS 5000 // Wait 5 seconds after a failed connection attempt


// Define Geiger
const int inputPin = 26;

volatile unsigned long counts = 0;                       // Tube events
int countsdif = 0;
float cps = 0;                                             // CPs
int lastCounts = 0;
unsigned long lastCountTime;                            // Time measurement
unsigned long startMillis;
unsigned long currentMillis;
unsigned long currentElapsed;

void IRAM_ATTR ISR_impulse() { // Captures count of events from Geiger counter board
  counts++;
}


// Configuration data for the BSEC library - telling it it is running on a 3.3v supply, that it is read from every 3 seconds, and that the sensor should take into account the last 4 days worth of data for calibration purposes.
const uint8_t bsec_config_iaq[] = {
#include "config/generic_33v_3s_4d/bsec_iaq.txt"
};

// Create an object of the class Bsec
Bsec iaqSensor;
String output;

// This stores a question mark and is eventually replaced with a space character once the BSEC library has decided it has enough calibration data to persist. It is displayed in the bottom right of the LCD as a kind of debug symbol.
String sensorPersisted = "?";
uint8_t bsecState[BSEC_MAX_STATE_BLOB_SIZE] = {0};
uint16_t stateUpdateCounter = 0;

WiFiClient espClient;
PubSubClient broker(espClient);

void setup(void)
{
  Serial.begin(115200);
  broker.setServer(mqtt_server, 1883);
  Wire.begin();

  //Setup BME688
  iaqSensor.begin(BME680_I2C_ADDR_PRIMARY, Wire);
  output = "\nBSEC library version " + String(iaqSensor.version.major) + "." + String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + String(iaqSensor.version.minor_bugfix);
  Serial.println(output);
  checkIaqSensorStatus();
  iaqSensor.setConfig(bsec_config_iaq);

  //loadState here refers to the BSEC Calibration state.
  loadState();

  // List all the sensors we want the bsec library to give us data for
  bsec_virtual_sensor_t sensorList[10] = {
    BSEC_OUTPUT_RAW_TEMPERATURE,
    BSEC_OUTPUT_RAW_PRESSURE,
    BSEC_OUTPUT_RAW_HUMIDITY,
    BSEC_OUTPUT_RAW_GAS,
    BSEC_OUTPUT_IAQ,
    BSEC_OUTPUT_STATIC_IAQ,
    BSEC_OUTPUT_CO2_EQUIVALENT,
    BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  // Receive data from sensor list above at BSEC_SAMPLE_RATE_LP rate (every 3 seconds). There is also BSEC_SAMPLE_RATE_ULP - which requires a configuration change above.
  iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);
  checkIaqSensorStatus();
  connectToNetwork();

/*  xTaskCreatePinnedToCore(
  keepWiFiAlive,
  "keepWiFiAlive",  // Task name
  5000,             // Stack size (bytes)
  NULL,             // Parameter
  1,                // Task priority
  NULL,             // Task handle
  ARDUINO_RUNNING_CORE
  );*/

  //Setup Geiger
  pinMode(inputPin, INPUT);                            // Set pin for capturing Tube events
  attachInterrupt(inputPin, ISR_impulse, FALLING);     // Define interrupt on falling edge
  startMillis = millis();
}


void loop(void)
{
  if (WiFi.status() != WL_CONNECTED) {
    connectToNetwork();
  }
  if (!broker.connected()) {
    reconnectToBroker();
  }
  broker.loop();
  getGeigerCounts();
  if (iaqSensor.run()) {
    displayIAQ(iaqSensor.staticIaq);
    displayIAQAcc(iaqSensor.iaqAccuracy);
    displayTemp(iaqSensor.temperature);
    displayHumidity(iaqSensor.humidity);
    displayPressure(iaqSensor.pressure);
    displayCO2(iaqSensor.co2Equivalent);
    displayVOC(iaqSensor.breathVocEquivalent);
    updateState();
  } else {
    checkIaqSensorStatus();
  }
}

void getGeigerCounts(){
  currentMillis = millis();
  currentElapsed = currentMillis - startMillis;
  if (currentElapsed >= 10000) {
    countsdif = counts - lastCounts;
    cps = countsdif * (1000.0/(currentElapsed/1.0)) ;
    lastCounts = counts;
    Serial.println(String(cps) + " cps");
    char carr[8];
    dtostrf(cps, 1, 2, carr);
    broker.publish("/bme688/cps", carr);
    startMillis = currentMillis;
  }
  
   
}

// The below display functions display data as well as publishing it to the MQTT broker. They expect the area that they are rendering to to be free of characters.
void displayIAQ(float iaq)
{
  Serial.println(iaq);
  char carr[8];
  dtostrf(iaq, 1, 2, carr);
  broker.publish("/bme688/iaq", carr);
}

void displayIAQAcc(float iaqacc)
{
  Serial.println(iaqacc);
  char carr[8];
  dtostrf(iaqacc, 1, 2, carr);
  broker.publish("/bme688/iaqacc", carr);
}

void displayTemp(float tmp)
{
  Serial.println(tmp);
  char carr[8];
  dtostrf(tmp, 1, 2, carr);
  broker.publish("/bme688/temperature", carr);
}

void displayHumidity(float humidity)
{
  Serial.println(String(humidity) + "%");
  char carr[8];
  dtostrf(humidity, 1, 2, carr);
  broker.publish("/bme688/humidity", carr);
}

void displayPressure(float pressure)
{
  Serial.println(pressure);
  char carr[12];
  dtostrf(pressure, 1, 2, carr);
  broker.publish("/bme688/pressure", carr);
}

void displayCO2(float co)
{
  Serial.println("CO2 " + String(co) + "ppm");
  char carr[8];
  dtostrf(co, 1, 2, carr);
  broker.publish("/bme688/co", carr);
}

void displayVOC(float voc)
{
  Serial.println("VOC " + String(voc) + "ppm");
  char carr[8];
  dtostrf(voc, 1, 2, carr);
  broker.publish("/bme688/voc", carr);
}


// checks to make sure the BME680 Sensor is working correctly.
void checkIaqSensorStatus(void)
{
  if (iaqSensor.status != BSEC_OK) {
    if (iaqSensor.status < BSEC_OK) {
      output = "BSEC error code : " + String(iaqSensor.status);
      Serial.println(output);
    } else {
      output = "BSEC warning code : " + String(iaqSensor.status);
      Serial.println(output);
    }
    delay(5000);
    checkIaqSensorStatus();
  }

  if (iaqSensor.bme680Status != BME680_OK) {
    if (iaqSensor.bme680Status < BME680_OK) {
      output = "BME680 error code : " + String(iaqSensor.bme680Status);
      Serial.println(output);
    } else {
      output = "BME680 warning code : " + String(iaqSensor.bme680Status);
      Serial.println(output);
    }
    delay(5000);
  }
}

void connectToNetwork() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, WiFiPassword);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Establishing connection to WiFi..");
  }
  Serial.println("Connected to network");
  delay(1000);
}


void reconnectToBroker() {
  // Loop until we're reconnected
  while (!broker.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "aqm-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (broker.connect(clientId.c_str())) {
      Serial.println("connected to broker");
      delay(1000);
    } else {
      Serial.print("failed connecting to broker, rc=");
      Serial.print(broker.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}





// loadState attempts to read the BSEC state from the EEPROM. If the state isn't there yet - it wipes that area of the EEPROM ready to be written to in the future.
void loadState(void)
{
  if (EEPROM.read(0) == BSEC_MAX_STATE_BLOB_SIZE) {
    // Existing state in EEPROM
    Serial.println("Reading state from EEPROM");

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE; i++) {
      bsecState[i] = EEPROM.read(i + 1);
      Serial.println(bsecState[i], HEX);
    }
    sensorPersisted = " ";
    iaqSensor.setState(bsecState);
    checkIaqSensorStatus();
  } else {
    // Erase the EEPROM with zeroes
    Serial.println("Erasing EEPROM");

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE + 1; i++)
      EEPROM.write(i, 0);

    EEPROM.commit();
  }
}

// updateState waits for the in air quality accuracy to hit '3' - and will then write the state to the EEPROM. Then on every STATE_SAVE_PERIOD, it'll update the state.
void updateState(void)
{
  bool update = false;
  /* Set a trigger to save the state. Here, the state is saved every STATE_SAVE_PERIOD with the first state being saved once the algorithm achieves full calibration, i.e. iaqAccuracy = 3 */
  if (stateUpdateCounter == 0) {
    if (iaqSensor.iaqAccuracy >= 3) {
      update = true;
      stateUpdateCounter++;
    }
  } else {
    /* Update every STATE_SAVE_PERIOD milliseconds */
    if ((stateUpdateCounter * STATE_SAVE_PERIOD) < millis()) {
      update = true;
      stateUpdateCounter++;
    }
  }

  if (update) {
    iaqSensor.getState(bsecState);
    checkIaqSensorStatus();

    Serial.println("Writing state to EEPROM");

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE ; i++) {
      EEPROM.write(i + 1, bsecState[i]);
      Serial.println(bsecState[i], HEX);
    }

    EEPROM.write(0, BSEC_MAX_STATE_BLOB_SIZE);
    EEPROM.commit();
  }
}


void keepWiFiAlive(void * parameter){
    for(;;){
        if(WiFi.status() == WL_CONNECTED){
            vTaskDelay(10000 / portTICK_PERIOD_MS);
            continue;
        }

        Serial.println("[WIFI] Connecting");
        WiFi.mode(WIFI_STA);
        WiFi.begin(SSID, WiFiPassword);

        unsigned long startAttemptTime = millis();

        // Keep looping while we're not connected and haven't reached the timeout
        while (WiFi.status() != WL_CONNECTED && 
                millis() - startAttemptTime < WIFI_TIMEOUT_MS){}

        // When we couldn't make a WiFi connection (or the timeout expired)
      // sleep for a while and then retry.
        if(WiFi.status() != WL_CONNECTED){
            Serial.println("[WIFI] FAILED");
            vTaskDelay(WIFI_RECOVER_TIME_MS / portTICK_PERIOD_MS);
        continue;
        }

        Serial.println("[WIFI] Connected: " + WiFi.localIP());
    }
}
