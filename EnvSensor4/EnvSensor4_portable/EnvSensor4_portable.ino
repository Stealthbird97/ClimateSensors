#include <PubSubClient.h>
#include "bsec.h"
#include <WiFi.h>
#include <EEPROM.h>
#include <BME280I2C.h>
#include <EnvironmentCalculations.h>

// Controls how often the code persists the BSEC Calibration data
#define STATE_SAVE_PERIOD  UINT32_C(60 * 60 * 1000) // every 60 minutes

const char* mqtt_server = "10.1.1.3";
const char *SSID = "Radio-Noise S";
const char *WiFiPassword = "z4!9$*3wb3xxpyv2@gio";
const char *device = "Portable";
#define WIFI_TIMEOUT_MS 10000 // 10 second WiFi connection timeout
#define WIFI_RECOVER_TIME_MS 5000 // Wait 5 seconds after a failed connection attempt

WiFiClient espClient;
PubSubClient broker(espClient);

BME280I2C::Settings settings(
   BME280::OSR_X16,
   BME280::OSR_X16,
   BME280::OSR_X16,
   BME280::Mode_Forced,
   BME280::StandbyTime_1000ms,
   BME280::Filter_Off,
   BME280::SpiEnable_False,
   0x76 // I2C address. I2C specific.
);

BME280I2C bme(settings);

void setup(void)
{
  Serial.begin(115200);
  broker.setServer(mqtt_server, 1883);
  Wire.begin();

  while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }

  // bme.chipID(); // Deprecated. See chipModel().
  switch(bme.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor! Success.");
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor! No Humidity available.");
       break;
     default:
       Serial.println("Found UNKNOWN sensor! Error!");
  }

  connectToNetwork();

/*  xTaskCreatePinnedToCore(
  keepWiFiAlive,
  "keepWiFiAlive",  // Task name
  5000,             // Stack size (bytes)
  NULL,             // Parameter
  1,                // Task priority
  NULL,             // Task handle
  ARDUINO_RUNNING_CORE
  );*/
}


void loop(void)
{
  if (!broker.connected()) {
    reconnectToBroker();
  }
  broker.loop();
  float temp(NAN), hum(NAN), pres(NAN);
  
  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  BME280::PresUnit presUnit(BME280::PresUnit_Pa);
  bme.read(pres, temp, hum, tempUnit, presUnit);
  displayTemp(temp);
  displayHumidity(hum);
  displayPressure(pres);
}
void displayTemp(float tmp)
{
  Serial.println(tmp);
  char carr[8];
  dtostrf(tmp, 1, 2, carr);
  broker.publish("/bme280/temperature", carr);
}

void displayHumidity(float humidity)
{
  Serial.println(String(humidity) + "%");
  char carr[8];
  dtostrf(humidity, 1, 2, carr);
  broker.publish("/bme280/humidity", carr);
}

void displayPressure(float pressure)
{
  Serial.println(pressure);
  char carr[12];
  dtostrf(pressure, 1, 2, carr);
  broker.publish("/bme280/pressure", carr);
}

void connectToNetwork() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, WiFiPassword);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Establishing connection to WiFi..");
  }
  Serial.println("Connected to network");
  delay(1000);
}


void reconnectToBroker() {
  // Loop until we're reconnected
  while (!broker.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "aqm-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (broker.connect(clientId.c_str())) {
      Serial.println("connected to broker");
      delay(1000);
    } else {
      Serial.print("failed connecting to broker, rc=");
      Serial.print(broker.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void keepWiFiAlive(void * parameter){
    for(;;){
        if(WiFi.status() == WL_CONNECTED){
            vTaskDelay(10000 / portTICK_PERIOD_MS);
            continue;
        }

        Serial.println("[WIFI] Connecting");
        WiFi.mode(WIFI_STA);
        WiFi.begin(SSID, WiFiPassword);

        unsigned long startAttemptTime = millis();

        // Keep looping while we're not connected and haven't reached the timeout
        while (WiFi.status() != WL_CONNECTED && 
                millis() - startAttemptTime < WIFI_TIMEOUT_MS){}

        // When we couldn't make a WiFi connection (or the timeout expired)
      // sleep for a while and then retry.
        if(WiFi.status() != WL_CONNECTED){
            Serial.println("[WIFI] FAILED");
            vTaskDelay(WIFI_RECOVER_TIME_MS / portTICK_PERIOD_MS);
        continue;
        }

        Serial.println("[WIFI] Connected: " + WiFi.localIP());
    }
}
